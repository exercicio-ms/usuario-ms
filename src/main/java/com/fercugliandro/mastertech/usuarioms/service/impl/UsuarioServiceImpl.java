package com.fercugliandro.mastertech.usuarioms.service.impl;

import com.fercugliandro.mastertech.usuarioms.client.Cep;
import com.fercugliandro.mastertech.usuarioms.client.ViaCepClient;
import com.fercugliandro.mastertech.usuarioms.model.Usuario;
import com.fercugliandro.mastertech.usuarioms.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private ViaCepClient cepClient;

    @Override
    public Usuario obterUsuario(String nome, String cep) {

        Cep cepRetorno = cepClient.obterCep(cep);
        if (cepRetorno == null)
            return null;

        Usuario usuario = new Usuario();
        usuario.setNome(nome);
        usuario.setCep(cepRetorno);


        return usuario;
    }
}
