package com.fercugliandro.mastertech.usuarioms.service;

import com.fercugliandro.mastertech.usuarioms.model.Usuario;

public interface UsuarioService {

    Usuario obterUsuario(String nome, String cep);

}
