package com.fercugliandro.mastertech.usuarioms;

import com.fercugliandro.mastertech.usuarioms.model.Usuario;
import com.fercugliandro.mastertech.usuarioms.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioAPI {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping("/usuario/{nome}/{cep}")
    public ResponseEntity<?> obterUsuario(@PathVariable String nome, @PathVariable String cep) {

        Usuario usuario = usuarioService.obterUsuario(nome, cep);

        return new ResponseEntity<>(usuario, HttpStatus.OK);
    }

}
