package com.fercugliandro.mastertech.usuarioms.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cep")
public interface ViaCepClient {

    @GetMapping("/cep/{cep}")
    Cep obterCep(@PathVariable String cep);
}
