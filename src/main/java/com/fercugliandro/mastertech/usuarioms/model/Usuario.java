package com.fercugliandro.mastertech.usuarioms.model;

import com.fercugliandro.mastertech.usuarioms.client.Cep;

public class Usuario {

    private String nome;
    private Cep cep;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Cep getCep() {
        return cep;
    }

    public void setCep(Cep cep) {
        this.cep = cep;
    }
}
